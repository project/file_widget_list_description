<?php

namespace Drupal\file_widget_list_description\TwigExtension;

use Drupal\Core\Template\TwigExtension;
use Drupal\Core\Entity\Entity\EntityFormDisplay;

/**
 * Class DefaultTwigExtension.
 *
 * @package Drupal\file_widget_list_description
 */
class DefaultTwigExtension extends TwigExtension {

  /**
   * {@inheritdoc}
   */
  public function getTokenParsers() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getNodeVisitors() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getFilters() {
    return [
      new \Twig_SimpleFilter('file_widget_list_description', array($this, 'transformKeyDescription')),
      new \Twig_SimpleFilter('format_size', 'format_size'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getTests() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getFunctions() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getOperators() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return 'file_widget_list_description.twig.extension';
  }

  /**
   * Translates a key into the description.
   */
  public function transformKeyDescription($key, $field_name, $entity_type, $bundle, $form_mode = 'default') {
    // Try loading the entity from configuration.
    $entity_form_display = EntityFormDisplay::load($entity_type . '.' . $bundle . '.' . $form_mode);

    if ($entity_form_display) {
      $allowed_values = $entity_form_display->getComponent($field_name)['settings']['allowed_values'];
      return isset($allowed_values[$key]) ? $allowed_values[$key] : $key;
    }
    return $key;
  }

}
