<?php

namespace Drupal\file_widget_list_description\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Plugin\Field\FieldWidget\FileWidget;
use Drupal\file_widget_list_description\AllowedValuesTrait;

/**
 * Plugin implementation of the 'file_list_description_widget' widget.
 *
 * @FieldWidget(
 *   id = "file_list_description_widget",
 *   label = @Translation("File with description list"),
 *   field_types = {
 *     "file"
 *   }
 * )
 */
class FileListDescriptionWidget extends FileWidget {

  use AllowedValuesTrait;

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'allowed_values' => [],
      'custom_title' => '',
      'custom_description' => '',
      'required' => FALSE,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = [];

    $allowed_values = $this->getSetting('allowed_values');

    $description = '<p>' . t('The possible values this field can contain. Enter one value per line, in the format key|label.');
    $description .= '<br/>' . t('The key is the stored value. The label will be used in displayed values and edit forms.');
    $description .= '<br/>' . t('The label is optional: if a line contains a single string, it will be used as key and label.');
    $description .= '</p>';

    $elements['allowed_values'] = [
      '#type' => 'textarea',
      '#title' => t('Allowed values list'),
      '#default_value' => $this->allowedValuesString($allowed_values),
      '#rows' => 10,
      '#element_validate' => [[get_class($this), 'validateAllowedValues']],
      '#allowed_values' => $allowed_values,
      '#description' => $description,
    ];

    $elements['custom_title'] = [
      '#type' => 'textfield',
      '#title' => t('Custom title for the description field'),
      '#default_value' => $this->getSetting('custom_title'),
    ];

    $elements['custom_description'] = [
      '#type' => 'textarea',
      '#title' => t('Custom description for the description field'),
      '#default_value' => $this->getSetting('custom_description'),
      '#rows' => 3,
      '#description' => t('Use &lt;none&gt; to leave it empty'),
    ];

    $elements['required'] = [
      '#type' => 'checkbox',
      '#title' => t('Make the description mandatory'),
      '#default_value' => $this->getSetting('required'),
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);
    if (isset($element['#process'])) {
      $element_info = $this->elementInfo->getInfo('managed_file');
      $element['#process'] = array_merge($element_info['#process'], [[get_class($this), 'process']]);
      // Add properties needed by process().
      $element['#file_widget_description_settings'] = $this->getSettings();
    }
    return $element;
  }

  /**
   * Form API callback: Processes a file_generic field element.
   *
   * Replaces the description textfield with a select widget.
   *
   * This method is assigned as a #process callback in formElement() method.
   */
  public static function process($element, FormStateInterface $form_state, $form) {
    $element = parent::process($element, $form_state, $form);
    if (isset($element['description'])) {
      // Get the description.
      $description = empty($element['#file_widget_description_settings']['custom_description']) ?
        $element['description']['#description'] :
        $element['#file_widget_description_settings']['custom_description'];
      // Empty it if <none> the user want no description.
      $description = $description === '<none>' ? '' : $description;
      $element['description'] = [
        '#type' => 'select',
        '#options' => $element['#file_widget_description_settings']['allowed_values'],
        '#value' => $element['description']['#value'],
        '#title' => empty($element['#file_widget_description_settings']['custom_title']) ? $element['description']['#title'] : $element['#file_widget_description_settings']['custom_title'],
        '#description' => $description,
        '#required' => $element['#file_widget_description_settings']['required'],
      ];
    }
    return $element;
  }

}
